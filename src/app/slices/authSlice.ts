import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";

const localAuthState = JSON.parse(localStorage.getItem("authState") as string);

export interface authState {
    status: "idle" | "loading" | "failed";
    isLoggedIn: boolean;
    token: string;
}

const initialState: authState = {
    status: "idle",
    isLoggedIn: localAuthState?.isLoggedIn ? true : false,
    token: localAuthState?.token ? localAuthState?.token : "",
};

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        setStatus: (state, action) => {
            state.status = action.payload;
        },
        signin: (state, action) => {
            localStorage.setItem("authState", JSON.stringify(action.payload));
            state.isLoggedIn = true;
            state.token = action.payload.token;
        },
        signout: (state) => {
            localStorage.removeItem("authState");
            state.isLoggedIn = false;
            state.token = "";
        },
    },
});

export const { setStatus, signin, signout } = authSlice.actions;

export const selectStatus = (state: RootState) => state.auth.status;
export const selectIsLoggedIn = (state: RootState) => state.auth.isLoggedIn;
export const selectToken = (state: RootState) => state.auth.token;

export default authSlice.reducer;

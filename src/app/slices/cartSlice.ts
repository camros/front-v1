import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import callApi from "src/api";
import { RootState } from "../store";
import { ProductType } from "./productsSlice";

export interface cartState {
    status: "idle" | "loading" | "failed";
    items: {
        product_count: number;
        product: ProductType;
    }[];
}

const initialState: cartState = {
    status: "idle",
    items: [],
};

export const fetchCartItems = createAsyncThunk(
    "cart/fetchCartItems",
    async (token: string) => {
        try {
            const response = await callApi("cart/", "GET", {}, token);

            return response.data;
        } catch (error) {
            return [];
        }
    },
);

export const addCartItem = createAsyncThunk(
    "cart/addCartItem",
    async (data: { token: string; id: string }) => {
        const { token, id } = data;

        try {
            const response = await callApi(
                `cart/add/product/${id}`,
                "POST",
                {},
                token,
            );

            return response.data;
        } catch (error) {}
    },
);

export const removeCartItem = createAsyncThunk(
    "cart/removeCartItem",
    async (data: { token: string; id: string }) => {
        const { token, id } = data;

        try {
            const response = await callApi(
                `cart/remove/product/${id}`,
                "POST",
                {},
                token,
            );

            return response.data;
        } catch (error) {}
    },
);

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addProduct: (state, action) => {
            const oldItemIndex = state.items.findIndex(
                (item) => item.product.id === action.payload.id,
            );

            if (oldItemIndex !== -1) {
                state.items[oldItemIndex].product_count++;
            } else {
                state.items.push({
                    product_count: 1,
                    product: action.payload,
                });
            }
        },
        removeProduct: (state, action) => {
            const oldItem = state.items.find(
                (item) => item.product.id === action.payload,
            );

            if (oldItem?.product_count === 1) {
                state.items = state.items.filter(
                    (item) => item.product.id !== oldItem.product.id,
                );
            } else {
                state.items = state.items.map((item) =>
                    item.product.id === oldItem?.product.id
                        ? {
                              product: item.product,
                              product_count: --item.product_count,
                          }
                        : item,
                );
            }
        },
        removeProducts: (state) => {
            state.items = [];
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchCartItems.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchCartItems.fulfilled, (state, action) => {
                state.status = "idle";
                state.items = action.payload;
            })
            .addCase(fetchCartItems.rejected, (state) => {
                state.status = "failed";
            });
    },
});

export const { addProduct, removeProduct, removeProducts } = cartSlice.actions;

export const selectStatus = (state: RootState) => state.cart.status;
export const selectCartItems = (state: RootState) => state.cart.items;

export default cartSlice.reducer;

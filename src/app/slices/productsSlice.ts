import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import callApi from "src/api";
import { RootState } from "../store";

export type ProductType = {
    id: string;
    title: string;
    description: string;
    photo: string;
    price: string;
};

export interface ProductsState {
    status: "idle" | "loading" | "failed";
    results: ProductType[];
}

const initialState: ProductsState = {
    status: "idle",
    results: [],
};

export const fetchProducts = createAsyncThunk(
    "products/fetchProducts",
    async () => {
        try {
            const response = await callApi("product/");

            return response.data.results;
        } catch (error) {
            return [];
        }
    },
);

export const productsSlice = createSlice({
    name: "products",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchProducts.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchProducts.fulfilled, (state, action) => {
                state.status = "idle";
                state.results = action.payload;
            })
            .addCase(fetchProducts.rejected, (state) => {
                state.status = "failed";
            });
    },
});

export const selectStatus = (state: RootState) => state.products.status;
export const selectProducts = (state: RootState) => state.products.results;

export default productsSlice.reducer;

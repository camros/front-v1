import axios from "axios";

export default function callApi(
    endpoint: string,
    method = "GET",
    body: any = {},
    token = "",
) {
    const options: any = {
        method,
        url: process.env.REACT_APP_API_BASE_URL + endpoint,
        data: body,
    };

    if (token) {
        options.headers = {
            Authorization: `token ${token}`,
        };
    }

    return axios(options);
}

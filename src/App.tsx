import React, { useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { fetchProducts } from "./app/slices/productsSlice";
import { fetchCartItems } from "./app/slices/cartSlice";
import { selectToken } from "./app/slices/authSlice";
import Header from "./components/Header";
import ProductsList from "./components/ProductsList";
import Cart from "./components/Cart";
import Signup from "./components/Signup";
import Signin from "./components/Signin";
import Payment from "./components/Payment";
import Footer from "./components/Footer";
import Sass from "./App.module.scss";
import NotFound from "./components/NotFound";

function App() {
    const dispatch = useAppDispatch();
    const token = useAppSelector(selectToken);

    useEffect(() => {
        dispatch(fetchProducts());

        if (token) {
            dispatch(fetchCartItems(token));
        }
    }, []);

    return (
        <div className={Sass.wrapper}>
            <Header />
            <main>
                <Routes>
                    <Route path="/" element={<ProductsList />}></Route>
                    <Route path="/products" element={<ProductsList />}></Route>
                    <Route path="/cart" element={<Cart />}></Route>
                    <Route path="/payment" element={<Payment />}></Route>
                    <Route path="/signup" element={<Signup />}></Route>
                    <Route path="/signin" element={<Signin />}></Route>
                    <Route path="/*" element={<NotFound />}></Route>
                </Routes>
            </main>
            <Footer />
        </div>
    );
}

export default App;

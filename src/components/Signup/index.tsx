import React, { ReactElement, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { SpinnerCircular } from "spinners-react";
import callApi from "src/api";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import { LatLng } from "leaflet";
import { Form, Button } from "react-bootstrap";
import {
    setStatus,
    selectIsLoggedIn,
    selectStatus,
} from "src/app/slices/authSlice";
import Card from "../Card";
import Map from "../Map";

const Signup = (): ReactElement => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const status = useAppSelector(selectStatus);

    const [formData, setFormData] = useState({
        email: "",
        password: "",
        confirm_password: "",
        address: "",
        location: {
            lat: 40.8704,
            lng: -89.5166,
        },
    });

    const [errors, setErrors] = useState({
        email: "",
        password: "",
        confirm_password: "",
        address: "",
    });

    useEffect(() => {
        if (isLoggedIn) {
            navigate("/");
        }
    }, []);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { name, value } = e.target;

        setFormData({ ...formData, [name]: value });
        setErrors({ ...errors, [name]: "" });
    };

    const handleSetLocation = (location: LatLng) => {
        setFormData({ ...formData, location });
    };

    const handleSubmit = (e: React.SyntheticEvent): void => {
        e.preventDefault();
        dispatch(setStatus("loading"));

        let containErrors = false;
        const newErrors = {
            email: "",
            password: "",
            confirm_password: "",
            address: "",
        };

        if (!/\S+@\S+\.\S+/.test(formData.email)) {
            newErrors.email = "Invalid Email!";
            containErrors = true;
        }

        if (formData.password.length < 5) {
            newErrors.password = "Use at least 5 characters.";
            containErrors = true;
        }

        if (formData.password !== formData.confirm_password) {
            newErrors.confirm_password = "It doesn't match with Password.";
            containErrors = true;
        }

        if (formData.address.length < 10) {
            newErrors.address = "Use at least 10 characters.";
            containErrors = true;
        }

        if (formData.address.length > 100) {
            newErrors.address = "It has more than 100 characters!";
            containErrors = true;
        }

        if (containErrors) {
            setErrors(newErrors);
            dispatch(setStatus("idle"));

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth",
            });
        } else {
            const register = async () => {
                try {
                    const { data } = await callApi("user/register", "POST", {
                        ...formData,
                    });

                    if (data?.id) {
                        dispatch(setStatus("idle"));
                        navigate("/signin");
                    }
                } catch (error: any) {
                    dispatch(setStatus("idle"));

                    const { data } = error.response;

                    if (data?.[0]) {
                        setErrors({ ...errors, password: data[0] });
                    } else if (data?.detail) {
                        setErrors({ ...errors, email: data.detail });
                    } else if (data?.email) {
                        setErrors({ ...errors, email: data.email?.[0] });
                    }
                }

                window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: "smooth",
                });
            };

            register();
        }
    };

    return (
        <Card title="Signup" compact>
            <Form onSubmit={handleSubmit} noValidate>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="email"
                        name="email"
                        value={formData.email}
                        isInvalid={!!errors.email}
                        onChange={handleChange}
                    />
                    <Form.Control.Feedback type="invalid">
                        {errors.email}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        name="password"
                        value={formData.password}
                        isInvalid={!!errors.password}
                        onChange={handleChange}
                    />
                    <Form.Control.Feedback type="invalid">
                        {errors.password}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="confirm_password">
                    <Form.Label>Password Confirmation</Form.Label>
                    <Form.Control
                        type="password"
                        name="confirm_password"
                        value={formData.confirm_password}
                        isInvalid={!!errors.confirm_password}
                        onChange={handleChange}
                    />
                    <Form.Control.Feedback type="invalid">
                        {errors.confirm_password}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="address">
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                        as="textarea"
                        rows={2}
                        name="address"
                        value={formData.address}
                        isInvalid={!!errors.address}
                        onChange={handleChange}
                    />
                    <Form.Control.Feedback type="invalid">
                        {errors.address}
                    </Form.Control.Feedback>
                </Form.Group>
                <Map
                    position={formData.location}
                    setPosition={handleSetLocation}
                />
                <div className="text-end mt-5">
                    <Button
                        variant="success"
                        type="submit"
                        disabled={status === "loading"}
                    >
                        {status === "loading" && (
                            <SpinnerCircular
                                size={30}
                                thickness={90}
                                color="#999"
                                secondaryColor="#eee"
                            />
                        )}
                        &nbsp; Submit
                    </Button>
                </div>
            </Form>
        </Card>
    );
};

export default Signup;

import React, { ReactElement } from "react";
import Sass from "./index.module.scss";

const Footer = (): ReactElement => {
    return (
        <footer className={Sass.container}>
            Shop Agent Prototype - Camros Tech &copy;2025
        </footer>
    );
};

export default Footer;

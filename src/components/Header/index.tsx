import React, { ReactElement } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Badge, Container, Nav, Navbar } from "react-bootstrap";
import callApi from "src/api";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import {
    signout,
    selectIsLoggedIn,
    selectToken,
} from "src/app/slices/authSlice";
import { selectCartItems } from "src/app/slices/cartSlice";
import icon from "./shop.svg";

const Header = (): ReactElement => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const token = useAppSelector(selectToken);
    const cartItems = useAppSelector(selectCartItems);

    const handleSignout = () => {
        const logout = async () => {
            await callApi("user/logout", "POST", {}, token);
        };

        dispatch(signout());
        logout();
        navigate("/signin");

        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
        });
    };

    const calculateTotalItems = () => {
        return cartItems.reduce((acc, cur) => acc + cur.product_count, 0);
    };

    return (
        <Navbar
            className="bg-primary"
            expand="lg"
            style={{ boxShadow: "0px 5px 10px 0px rgba(100, 100, 111, 0.2)" }}
        >
            <Container>
                <Navbar.Brand>
                    <Nav.Link className="link-light" as={Link} to="/">
                        <img
                            alt="logo"
                            src={icon}
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                            style={{ position: "relative", top: "-3px" }}
                        />
                        &nbsp; Online Shop
                    </Nav.Link>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbar" />
                <Navbar.Collapse id="navbar">
                    <Nav className="ms-auto">
                        {isLoggedIn ? (
                            <>
                                <Nav.Link
                                    className="link-light"
                                    as={Link}
                                    to="/products"
                                >
                                    Products
                                </Nav.Link>
                                <Nav.Link
                                    className="link-light"
                                    as={Link}
                                    to="/cart"
                                >
                                    Cart &nbsp;
                                    {Array.isArray(cartItems) &&
                                    cartItems.length > 0 ? (
                                        <Badge bg="warning" text="dark">
                                            {calculateTotalItems()}
                                        </Badge>
                                    ) : null}
                                </Nav.Link>
                                <Nav.Link
                                    className="link-light"
                                    onClick={handleSignout}
                                >
                                    Signout
                                </Nav.Link>
                            </>
                        ) : (
                            <>
                                <Nav.Link
                                    className="link-light"
                                    as={Link}
                                    to="/signin"
                                >
                                    Signin
                                </Nav.Link>
                                <Nav.Link
                                    className="link-light"
                                    as={Link}
                                    to="/signup"
                                >
                                    Signup
                                </Nav.Link>
                            </>
                        )}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default Header;

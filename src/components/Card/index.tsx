import React, { ReactElement } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Sass from "./index.module.scss";

type CardProps = {
    title: string;
    children: ReactElement;
    compact?: boolean;
};

const Card = (props: CardProps): ReactElement => {
    return (
        <Container>
            <Row className="justify-content-center">
                <Col
                    sm={props.compact ? "8" : "12"}
                    md={props.compact ? "6" : "12"}
                >
                    <div className={Sass.card}>
                        <h1 className={Sass.heading}>{props.title}</h1>
                        {props.children}
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default Card;

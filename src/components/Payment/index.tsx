import React, { ReactElement, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { useAppDispatch } from "src/app/hooks";
import { removeProducts } from "src/app/slices/cartSlice";
import Card from "../Card";

const Payment = (): ReactElement => {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(removeProducts());

        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
        });
    }, []);

    return (
        <Card title="Payment Confirmation">
            <div className="text-center">
                <p>Successful Payment</p>
                <p className="mb-5">Thank you!</p>
                <Link to="/">
                    <Button variant="success">Back to Shop</Button>
                </Link>
            </div>
        </Card>
    );
};

export default Payment;

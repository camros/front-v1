import React, { ReactElement } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import Card from "../Card";

const NotFound = (): ReactElement => {
    return (
        <Card title="404 Error">
            <div className="text-center">
                <p className="mb-5">Unfortunately we couldn't find the page!</p>
                <Link to="/">
                    <Button variant="success">Back to Shop</Button>
                </Link>
            </div>
        </Card>
    );
};

export default NotFound;

import React, { ReactElement } from "react";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import { selectToken } from "src/app/slices/authSlice";
import { ProductType } from "src/app/slices/productsSlice";
import { addProduct, addCartItem } from "src/app/slices/cartSlice";
import Sass from "./index.module.scss";
import icon from "./plus.svg";

const Product = (props: ProductType): ReactElement => {
    const dispatch = useAppDispatch();
    const token = useAppSelector(selectToken);

    const handleAddToCart = async () => {
        dispatch(addCartItem({ token, id: props.id }));

        dispatch(
            addProduct({
                ...props,
            }),
        );

        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
        });
    };

    return (
        <div className={Sass.card}>
            <img className={Sass.image} src={props.photo} alt="samsung" />
            <div className={Sass.content}>
                <h4 className={Sass.title}>{props.title}</h4>
                <p className={Sass.description}>{props.description}</p>
            </div>
            <div className={Sass.actions}>
                <p className={Sass.price}>
                    <span className={Sass.currency}>$</span>
                    <strong className={Sass.value}>{props.price}</strong>
                </p>
                <button
                    className={Sass.add_btn}
                    type="button"
                    onClick={handleAddToCart}
                >
                    <img src={icon} alt="plus icon" />
                </button>
            </div>
        </div>
    );
};

export default Product;

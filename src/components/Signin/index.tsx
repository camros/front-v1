import React, { ReactElement, useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { SpinnerCircular } from "spinners-react";
import callApi from "src/api";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import {
    setStatus,
    signin,
    selectIsLoggedIn,
    selectStatus,
} from "src/app/slices/authSlice";
import Card from "../Card";

const Signin = (): ReactElement => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const status = useAppSelector(selectStatus);

    const [formData, setFormData] = useState({
        email: "",
        password: "",
    });

    const [errors, setErrors] = useState({
        email: "",
        password: "",
    });

    useEffect(() => {
        if (isLoggedIn) {
            navigate("/");
        }
    }, []);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { name, value } = e.target;

        setFormData({ ...formData, [name]: value });
        setErrors({ ...errors, [name]: "" });
    };

    const handleSubmit = (e: React.SyntheticEvent): void => {
        e.preventDefault();
        dispatch(setStatus("loading"));

        let containErrors = false;
        const newErrors = {
            email: "",
            password: "",
        };

        if (!/\S+@\S+\.\S+/.test(formData.email)) {
            newErrors.email = "Invalid Email!";
            containErrors = true;
        }

        if (formData.password.length < 5) {
            newErrors.password = "Use at least 5 characters.";
            containErrors = true;
        }

        if (containErrors) {
            setErrors(newErrors);
            dispatch(setStatus("idle"));

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth",
            });
        } else {
            const login = async () => {
                try {
                    const { data } = await callApi(
                        "user/login",
                        "POST",
                        formData,
                    );

                    if (data?.access_token) {
                        dispatch(
                            signin({
                                isLoggedIn: true,
                                token: data.access_token?.key,
                            }),
                        );

                        dispatch(setStatus("idle"));
                        navigate("/");
                    }
                } catch (error: any) {
                    dispatch(setStatus("idle"));

                    const { data } = error.response;

                    if (data?.[0]) {
                        setErrors({ ...errors, password: data[0] });
                    } else if (data?.detail) {
                        setErrors({ ...errors, email: data.detail });
                    }
                }
            };

            login();

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth",
            });
        }
    };

    return (
        <Card title="Signin" compact>
            <Form onSubmit={handleSubmit} noValidate>
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="email"
                        name="email"
                        value={formData.email}
                        isInvalid={!!errors.email}
                        onChange={handleChange}
                    />
                    <Form.Control.Feedback type="invalid">
                        {errors.email}
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        name="password"
                        value={formData.password}
                        isInvalid={!!errors.password}
                        onChange={handleChange}
                    />
                    <Form.Control.Feedback type="invalid">
                        {errors.password}
                    </Form.Control.Feedback>
                </Form.Group>
                <div className="text-end mt-5">
                    <Button
                        variant="success"
                        type="submit"
                        disabled={status === "loading"}
                    >
                        {status === "loading" && (
                            <SpinnerCircular
                                size={30}
                                thickness={90}
                                color="#999"
                                secondaryColor="#eee"
                            />
                        )}
                        &nbsp; Login
                    </Button>
                </div>
            </Form>
        </Card>
    );
};

export default Signin;

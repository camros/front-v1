import React, { ReactElement, useEffect } from "react";
import { Container, Col, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { SpinnerCircular } from "spinners-react";
import { useAppSelector } from "src/app/hooks";
import { selectIsLoggedIn } from "src/app/slices/authSlice";
import { selectProducts, selectStatus } from "src/app/slices/productsSlice";
import { ProductType } from "src/app/slices/productsSlice";
import Product from "../Product";

export type Products = {
    id: string;
    title: string;
    description: string;
    photo: string;
    price: string;
}[];

const ProductsList = (): ReactElement => {
    const navigate = useNavigate();
    const status = useAppSelector(selectStatus);
    const products = useAppSelector(selectProducts);
    const isLoggedIn = useAppSelector(selectIsLoggedIn);

    useEffect(() => {
        // if (!isLoggedIn) {
        //     navigate("/signin");
        // }
    }, []);

    return (
        <Container>
            <Row>
                {status === "loading" && (
                    <Col className="text-center">
                        <SpinnerCircular
                            thickness={180}
                            color="#5e5df0"
                            secondaryColor="#eee"
                        />
                    </Col>
                )}

                {status === "idle" &&
                    Array.isArray(products) &&
                    products.length > 0 && (
                        <>
                            {products.map((product: ProductType) => (
                                <Col
                                    sm="12"
                                    md="6"
                                    xl="4"
                                    xxl="3"
                                    key={product.id}
                                >
                                    <Product
                                        id={product.id}
                                        title={product.title}
                                        description={product.description}
                                        photo={product.photo}
                                        price={product.price}
                                    />
                                </Col>
                            ))}
                        </>
                    )}
            </Row>
        </Container>
    );
};

export default ProductsList;

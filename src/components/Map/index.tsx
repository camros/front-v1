import React, { ReactElement } from "react";
import L, { LatLng, LatLngExpression } from "leaflet";
import { MapContainer, TileLayer } from "react-leaflet";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import "leaflet/dist/leaflet.css";
import LocationFinder from "./LocationFinder";
import { Button } from "react-bootstrap";

let DefaultIcon = L.icon({
    iconUrl: icon,
    shadowUrl: iconShadow,
});

L.Marker.prototype.options.icon = DefaultIcon;

type MapProps = {
    position: LatLngExpression;
    setPosition: (position: LatLng) => void;
};

const Map = (props: MapProps): ReactElement => {
    const handleClick = () => {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (e) {
                props.setPosition({
                    lat: e.coords.latitude,
                    lng: e.coords.longitude,
                } as LatLng);
            });
        }
    };

    return (
        <div id="map" className="map">

            <MapContainer zoom={5} minZoom={5} center={[40.8704, -89.5166]}>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                />
                <LocationFinder
                    position={props.position}
                    setPosition={props.setPosition}
                />
            </MapContainer>
            <div className="mt-3 mb-1">
                <Button size="sm" onClick={handleClick}>
                    Current Location
                </Button>
            </div>
        </div>
    );
};

export default Map;

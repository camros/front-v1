import React, { ReactElement, useEffect } from "react";
import L, { LatLng, LatLngExpression } from "leaflet";
import { Marker, Popup, useMapEvents, useMap } from "react-leaflet";

type MapProps = {
    position: LatLngExpression;
    setPosition: (position: LatLng) => void;
};

const LocationFinder = (props: MapProps): ReactElement => {
    const map = useMap();

    useEffect(() => {
        map.locate().on("locationfound", function (e) {
            props.setPosition(e.latlng);
            map.flyTo(e.latlng, map.getZoom());
        });
    }, [map]);

    useMapEvents({
        click: (e) => {
            props.setPosition(e.latlng);
        },
    });

    return props.position === undefined ? (
        <></>
    ) : (
        <Marker position={props.position}>
            <Popup>You are here</Popup>
        </Marker>
    );
};

export default LocationFinder;

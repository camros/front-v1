import React, { ReactElement, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Button, Table } from "react-bootstrap";
import { SpinnerCircular } from "spinners-react";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import { selectIsLoggedIn, selectToken } from "src/app/slices/authSlice";
import {
    selectStatus,
    selectCartItems,
    removeCartItem,
    removeProduct,
} from "src/app/slices/cartSlice";
import Card from "../Card";
import Sass from "./index.module.scss";
import icon from "./trash.svg";

const Cart = (): ReactElement => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const token = useAppSelector(selectToken);
    const cartItems = useAppSelector(selectCartItems);
    const status = useAppSelector(selectStatus);

    useEffect(() => {
        if (!isLoggedIn) {
            navigate("/signin");
        }
    }, []);

    const handleRemoveItem = (id: string) => {
        dispatch(removeCartItem({ token, id }));
        dispatch(removeProduct(id));

        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
        });
    };

    const calculateTotal = () => {
        return cartItems.reduce(
            (acc, cur) => acc + Number(cur.product.price) * cur.product_count,
            0,
        );
    };

    return (
        <Card title="Your Cart">
            <>
                {status === "loading" && (
                    <div className="text-center">
                        <SpinnerCircular
                            thickness={180}
                            color="#5e5df0"
                            secondaryColor="#eee"
                        />
                    </div>
                )}
                {status === "idle" &&
                    Array.isArray(cartItems) &&
                    cartItems.length > 0 && (
                        <div>
                            <Table responsive>
                                <thead>
                                    <tr className="text-center">
                                        <th>#</th>
                                        <th className="text-start">Product</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {status === "idle" &&
                                        Array.isArray(cartItems) && (
                                            <>
                                                {cartItems.map(
                                                    (item, index) => (
                                                        <tr key={index}>
                                                            <td className="align-middle text-center">
                                                                {index + 1}
                                                            </td>
                                                            <td>
                                                                <img
                                                                    className={
                                                                        Sass.image
                                                                    }
                                                                    src={
                                                                        item
                                                                            ?.product
                                                                            ?.photo
                                                                    }
                                                                    alt={
                                                                        item
                                                                            ?.product
                                                                            ?.title
                                                                    }
                                                                />
                                                                <span>
                                                                    {
                                                                        item
                                                                            ?.product
                                                                            ?.title
                                                                    }
                                                                </span>
                                                            </td>
                                                            <td className="align-middle text-center">
                                                                {
                                                                    item?.product_count
                                                                }
                                                            </td>
                                                            <td className="align-middle text-center">
                                                                {`$${item?.product?.price}`}
                                                            </td>
                                                            <td className="align-middle text-center">
                                                                {`$${
                                                                    Number(
                                                                        item
                                                                            ?.product
                                                                            ?.price,
                                                                    ) *
                                                                    item?.product_count
                                                                }`}
                                                            </td>
                                                            <td className="align-middle text-center">
                                                                <button
                                                                    className={
                                                                        Sass.delete_btn
                                                                    }
                                                                    type="button"
                                                                    onClick={() =>
                                                                        handleRemoveItem(
                                                                            item
                                                                                ?.product
                                                                                ?.id,
                                                                        )
                                                                    }
                                                                >
                                                                    <img
                                                                        src={
                                                                            icon
                                                                        }
                                                                        alt="plus icon"
                                                                    />
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    ),
                                                )}

                                                <tr
                                                    className="bg-light"
                                                    key="total"
                                                >
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <th className="align-middle text-center">Total</th>
                                                    <td className="align-middle text-center">
                                                        {`$${calculateTotal()}`}
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </>
                                        )}
                                </tbody>
                            </Table>
                            <div className={Sass.actions}>
                                <Link to="/payment">
                                    <Button variant="success">Checkout</Button>
                                </Link>
                            </div>
                        </div>
                    )}

                {status === "idle" &&
                    Array.isArray(cartItems) &&
                    cartItems.length === 0 && (
                        <div className="text-center">
                            <p className="mb-5">Your cart is empty!</p>
                            <Link to="/">
                                <Button variant="success">Back to Shop</Button>
                            </Link>
                        </div>
                    )}
            </>
        </Card>
    );
};

export default Cart;
